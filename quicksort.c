#include <stdlib.h>
#include <stdio.h>

/*funcion que realizara el intercambiar en los valores*/
void cambio(int* a, int* b) {
    int temp = *a;
/*Aqui se tienen las direcciones de los elementos originales y imprime con el uso de la biblioteca stdio*/    
    printf("el valor %d", *a);
    printf(" en la pocicion=%p\n", a);
    printf("cambiara con el valor %d", *b);
    printf(" en la pocicion=%p\n", b);
    printf("\n\n");
    *a = *b;
    *b = temp;
/*Aqui se an cambiado las direcciones de los elementos y imprime con el uso de la biblioteca stdio*/    
    printf("el valor %d", *b);    
    printf(" cambio a la pocicion=%p\n", b);
    printf("el valor %d", *a);    
    printf(" cambio a la pocicion=%p\n", a);
    printf("\n\n");
  
}

/*funcion que ara las llamadas recursivas del algoritmo se usaran operaciones de apuntador y pocion sobre arreglos*/
void quicksort(int* izq, int* der) {
    if (der < izq)
        return;
    int pivote = *izq;
    int* ultimo = der;
    int* primero = izq;
    while (izq < der) {
        while (*izq <= pivote && izq < der+1)
            izq++;
        while (*der > pivote && der > izq-1)
            der--;
        if (izq < der)
            cambio(izq, der);
    }
    cambio(primero, der);
    quicksort(primero, der-1);
    quicksort(der+1, ultimo);
}

int main(void) {
    int i;
    int tam;

    /*definimos el tamaño del arreglo haciendo uso se scanf de la biblioteca estandar*/
    printf("Ingrese el numero de elementos del arreglo:\n");
    scanf("%d", &tam);
    int arreglo[tam];

    /*llenamos el arreglo de nueva cuenta con scanf*/
    printf("Ingrese los enteros para formar el arreglo:\n");
    for (i = 0; i < tam; i++)
        scanf("%d", &arreglo[i]);
    printf("n");

    /*mostramos el arreglo original*/
    printf("Arreglo Original:\n");
    for (i = 0; i < tam; i++)
        printf("%d ", arreglo[i]);
    printf("\n\n");

    /*Una vez formado el arreglo llamamos a la funcion recursiva quicksort*/
    quicksort(&arreglo[0], &arreglo[tam-1]);

    /*mostramos el arreglo una vez ordenado mediante la funcion printf */
    printf("Arreglo Ordenado:\n");
    for (i = 0; i < tam; i++)
        printf("%d ", arreglo[i]);
    printf("\n\n");
}