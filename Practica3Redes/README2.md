Ejercicio2

La infraestructura de clave publica (PKI) es la combinación del hardware, software y politicas y protocolos de seguridad para brindar las garantías del cifrado, firmas digitales y transacciones electrónicas.

El PKI permite la identificación de usuarios frente a otros usuarios para poder cifrar y descifrar mensajes entre si, entre otras operaciones de seguridad.

Se puede ver el RKI en las siguientes partes:
	-Un usuario identificador de la operación.
	-Sistemas de servidores que avalan la ocurrencia de la operación y garantizan la validez de los 		 certificados implicados en la operación.
	-Un destinatario de los datos cifrados/firmados/enviados garantizados por parte del usuario 	  	 iniciador de la operación.


Los sistemas PKI  son usados con mas regularidad en:
	Cifrado y/o autenticación de mensajes de correo electrónico.
Cifrado y/o autenticación de documentos. 
Autenticación de usuarios o aplicaciones 
Bootstraping  de protocolos seguros de comunicación,
