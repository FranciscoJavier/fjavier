Ejercicio 1

Deffie-Hellman definen un protocolo de establecimiento de llaves entre dos elementos que no hay
tenido una interacción con anterioridad haciendo uso de un canal no seguro y hacerlo de manera
privada.

La garantía de seguridad la otorga la complejidad del calculo de las operaciones empleadas, tiene como
principio la generación de claves entre dos componentes sin la la intervención de un tercero no autorizado  
que este escuchando para hacerse con la comunicación.

El algoritmo funciona con la toma de un par de números públicos y un privado por parte de cada 
interlocutor, cada elemento realiza ciertas operaciones con estos números, para después proceder a un
intercambio de resultados al revivirlos una ves mas hacen operaciones internas publicas y privadas con
ese conjunto de números, para que al final se verifique el resultado de esas operaciones dieron los 
mismos resultados los cuales servirán como clave.