//Llamada a librerías
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
 

main(int argc, char **argv[])
{
//Declaramos variablespara lectura
int fd, s;
char c;
//ABRIR ARCHIVO
fd = open("/dev/random",O_RDONLY);
//Leemos solamente un byte del archivo
s = read(fd,&c,1);
//rescatamos el valor generado de el archivo random para usarlo como semilla
long a = (long)&s;
//usamos la semilla para crear el numero aleatorio
int random = (int) ((4*a)%6);
//imprimimos el numero
printf(" El numero aleatorio es: %d .\n", random);
//Cerramos el archivo
close(fd);

}